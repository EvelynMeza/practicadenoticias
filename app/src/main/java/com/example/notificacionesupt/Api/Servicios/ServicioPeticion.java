package com.example.notificacionesupt.Api.Servicios;

import com.example.notificacionesupt.ViewModels.Peticion_Login;
import com.example.notificacionesupt.ViewModels.Peticion_Noticia;
import com.example.notificacionesupt.ViewModels.Registro_Noticia;
import com.example.notificacionesupt.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.GET;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @GET("api/todasNot")
    Call<Peticion_Noticia> getNoticias();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<Registro_Noticia> getRNoticias (@Field("usuarioId") int correo, @Field("titulo") String titu, @Field("descripcion") String descrip );
}
