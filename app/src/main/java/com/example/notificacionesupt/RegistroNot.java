package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Noticia;
import com.example.notificacionesupt.ViewModels.Registro_Noticia;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistroNot extends AppCompatActivity {
    private EditText ed7, ed8, ed9;
    private Button btn9,btn8;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro_not);
        ed7 = (EditText) findViewById(R.id.editText7);
        ed8 = (EditText) findViewById(R.id.editText8);
        ed9 = (EditText) findViewById(R.id.editText9);
        btn9 = (Button) findViewById(R.id.button9);
        btn8 = (Button) findViewById(R.id.button8);

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                startActivity(intent);
            }
        });
        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion s = Api.getApi(RegistroNot.this).create(ServicioPeticion.class);
                int renot = Integer.parseInt(ed7.getText().toString());
                Call<Registro_Noticia> R = s.getRNoticias(renot, ed8.getText().toString(), ed9.getText().toString());
                R.enqueue(new Callback<Registro_Noticia>() {
                    @Override
                    public void onResponse(Call<Registro_Noticia> call, Response<Registro_Noticia> response) {
                        Registro_Noticia mos = response.body();
                        if (mos.estado.equals("true")) {
                            Toast.makeText(RegistroNot.this, "Registro con Exito", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Registro_Noticia> call, Throwable t) {

                    }
                });
            }
        });

    }

}
