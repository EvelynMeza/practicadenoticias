package com.example.notificacionesupt.ViewModels;

import com.example.notificacionesupt.Not;

import java.util.List;

public class Peticion_Noticia {


    public String estado;
    public List<Not> detalle;


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Not> getDetalle() {
        return detalle;
    }

    public void setDetalle(List<Not> detalle) {
        this.detalle = detalle;
    }
}
