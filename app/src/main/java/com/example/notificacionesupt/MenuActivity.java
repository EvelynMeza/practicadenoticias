package com.example.notificacionesupt;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.notificacionesupt.Api.Api;
import com.example.notificacionesupt.Api.Servicios.ServicioPeticion;
import com.example.notificacionesupt.ViewModels.Peticion_Noticia;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MenuActivity extends AppCompatActivity {
    private String noticias;
    private Button btn5;
    private Button btn6;
    private ListView Listas;
    private String APITOKEN;
    private Button btn7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        btn5 = (Button) findViewById(R.id.button5);
        btn6=(Button)findViewById(R.id.button6);
        Listas=(ListView)findViewById(R.id.Lista);
        btn7=(Button)findViewById(R.id.button7);
        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(),RegistroNot.class);
                startActivity(intent);
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
                String token = APITOKEN;
                SharedPreferences.Editor editor = preferencias.edit();
                editor.putString("TOKEN", "");
                editor.commit();
                Intent intent = new Intent (getApplicationContext(),MainActivity.class);
                startActivity(intent);
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
                @Override
            public void onClick(View v) {
                ServicioPeticion s = Api.getApi(MenuActivity.this).create(ServicioPeticion.class);
                Call<Peticion_Noticia> R = s.getNoticias();
                R.enqueue(new Callback<Peticion_Noticia>() {
                    @Override
                    public void onResponse(Call<Peticion_Noticia> call, Response<Peticion_Noticia> response) {
                        Peticion_Noticia mos = response.body();
                        String Cad = "";
                        ArrayList<String> ev = new ArrayList<String>();
                        if (mos.estado.equals("true")) {
                            for (Not elemento : mos.detalle
                            ) {

                                Cad = Cad + elemento.id + " -- " + elemento.titulo + " -- " + elemento.descripcion + " -- " + elemento.created_at + "__" + elemento.update_at + "__" + "\n";


                            }
                            ev.add(Cad);
                            ArrayAdapter adaptador = new ArrayAdapter(MenuActivity.this, android.R.layout.simple_list_item_1, ev);
                            Listas.setAdapter(adaptador);


                        }
                    }

                    @Override
                    public void onFailure(Call<Peticion_Noticia> call, Throwable t) {

                    }
                });
            }
        });
            }


    }

